
const ctx = {
	roundabout:"ROUNDABOUT"
};


function concat(ctx, ...params) {
            return params.join('');
}

exports.helpers = [["concat", concat]];
exports.blockHelpers = [];
exports.ctx = ctx;
exports.description = "Deep paranthesis nesting test";