function makeURI(ctx, course, year, quarter) {
   const quarterId = quarter.slice(0, 2).toLowerCase();
   return `https://${ctx.domain}/${course}/${year%100}${quarterId}`;
}

function concat(ctx, ...params) {
   return params.join("");
}

function skip(ctx, body) {
    return "";
}

const ctx = {
    title: "Intro to Compiler Construction",
    domain: "www.cs.washington.edu/education/courses",
    year: "2018",
    quarter: "Winter"
};

exports.helpers = [
    ["makeURI", makeURI],
    ["concat", concat],
];
exports.blockHelpers = [
    ["skip", skip],
];
exports.ctx = ctx;
exports.description = "Allow nullary block helpers (0 arguments)";
