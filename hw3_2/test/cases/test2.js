
const ctx = {
  heroes: [
    {name:"Iron Man"},
    {name:"Captain America"},
    {name:"Hulk"},
    {name:"Thor"},
    {name:"Black Widow"},
    {name:"Black Panther"}

  ]
};

function addCharacter(ctx, name,char) {
  return name + char;
}

function thisIsABlockForTestingPurposes(ctx, body, hero, anothercharacter) {
	return hero + anothercharacter;
}

function eq(ctx, hero ,test){
	return hero == test;
}

exports.helpers = [["addCharacter", addCharacter], ["eq", eq]];
exports.blockHelpers = [["thisIsABlockForTestingPurposes", thisIsABlockForTestingPurposes]];
exports.ctx = ctx;
exports.description = "Implemented block helpers with arguments";