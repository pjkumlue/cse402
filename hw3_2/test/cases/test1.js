
const ctx = {
  heroes: [
    "Iron Man",
    "Captain America",
    "Hulk",
    "Thor",
    "Black Widow",
    "Black Panther"

  ]
};

function eq(ctx, test) {
  return ctx == test;
}

exports.helpers = [["eq", eq]];
exports.blockHelpers = [];
exports.ctx = ctx;
exports.description = "mismatched block helpers, we SHOULD fail";
