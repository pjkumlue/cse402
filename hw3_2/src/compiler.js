const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

function each(ctx, renderer, lst) {
  let str = "";
  for (let i = 0; i < lst.length; i++) {
    str += renderer(lst[i]);
  }
  return str;
}

function ifExpr(ctx, renderer, e) {
  if (e) {
    return renderer(ctx);
  }

  return "";
}

function withExpr(ctx, renderer, fieldName) {
  let v = ctx[fieldName];
  let str = renderer(v);
  return str;
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";

        this._helpers = {expr: {}, block: {}};
        this._used = {expr: [], block: []};
        this._bodyStack = [];
        this._salt = "__$";
        this._blockNameStack = [];
        this._blockArgsStack = [];

        this.registerBlockHelper("each", each);
        this.registerBlockHelper("if", ifExpr);
        this.registerBlockHelper("with", withExpr);
    }

    pushScope() {
      let str = `var ${this._outputVar} = "";\n`;
      this._bodyStack.push(str);
    }

    popScope() {
      let str = this._bodyStack.pop();
      str += `return ${this._outputVar};\n`;
      return str;
    }

    peek() {
      return this._bodyStack[this._bodyStack.length - 1];
    }


    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        //this._bodySource = `var ${this._outputVar} = "";\n`;
        this._bodyStack = [];
        this._blockNameStack = [];
        this._blockArgsStack = [];
        this._used = {expr: [], block: []}; 
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        for (var i = 0; i < this._used.expr.length; i++) {
          // TODO check for repeats with this._usedHelper
          let str = `var ${this._salt}${this._used.expr[i]} = ${this._helpers.expr[this._used.expr[i]].toString()};\n`;
           //this._bodySource = str.concat(this._bodySource);
          this._bodyStack[this._bodyStack.length - 1] = str.concat(this.peek());
        }

        for (var i = 0; i < this._used.block.length; i++) {
          // TODO check for repeats with this._usedHelper
          let str = `var ${this._salt}${this._used.block[i]} = ${this._helpers.block[this._used.block[i]].toString()};\n`;
           //this._bodySource = str.concat(this._bodySource);
          this._bodyStack[this._bodyStack.length - 1] = str.concat(this.peek());
        }


        let res = this.popScope();
        //console.log(this._bodyStack);
        //console.log(res + "\n");
        return new Function(this._inputVar, res);
        // this._bodySource += `return ${this._outputVar};\n`;
        // return new Function(this._inputVar, this._bodySource);
    }

    append(expr) {
        //this._bodySource += `${this._outputVar} += ${expr};\n`;
        this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`;
    }

    enterEndBlockElement(ctx) {
      let blockName = this._blockNameStack.pop();
      if (blockName != ctx.id.text) {
        throw `Block start '${blockName}' does not match the block end '${ctx.id.text}'.`
      }

      let res = this.popScope();
      //console.log(res);
      let func = new Function(this._inputVar, res);


      let expr = `${this._salt}${ctx.id.text}(${this._inputVar}, ${func.toString()}`;
      let args = this._blockArgsStack.pop();
      for (let i = 0; i < args.length; i++) {
        expr += `, ${args[i]} `;
      }

      expr += ")";
      this.append(expr);
    }

    exitStartBlockElement(ctx) {
      this.pushScope();
      this._blockNameStack.push(ctx.id.text);
      if (!this._used.block.includes(ctx.id.text)) {
        this._used.block.push(ctx.id.text);
      }
      let arr = [];
      for (let i =0; i <ctx.args.length; i++) {
        arr.push(ctx.args[i].source);
      }
      this._blockArgsStack.push(arr);
    }


    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitLiteral(ctx) {
        ctx.source = ctx.getText();
    }

    exitDataHelper(ctx) {
      var str = ctx.id.text;
      if (this._helpers.expr[str] != null) {
        if (!this._used.expr.includes(str)) {
          this._used.expr.push(str);
        }
        //helper
        let arr = ctx.args;
        ctx.source = `${this._salt}${ctx.id.text}(${this._inputVar}`;
        for (let i = 0; i < arr.length; i++) {
          //console.log(ctx.args[i].source);
          ctx.source += `, ${ctx.args[i].source}`;
         }
        ctx.source += ")";
      } else {
        ctx.source = `${this._inputVar}.${ctx.getText()}`;
      }
    }

    exitHelperCall(ctx) {
    var str = ctx.id.text;
      if (this._helpers.expr[str] != null) {
        if (!this._used.expr.includes(str)) {
          this._used.expr.push(str);
        }
        //helper
        let arr = ctx.args;
        ctx.source = `${this._salt}${ctx.id.text}(${this._inputVar}`;
        for (let i = 0; i < arr.length; i++) {
          //console.log(ctx.args[i].source);
          ctx.source += `, ${ctx.args[i].source}`;
         }
        ctx.source += ")";
      }
    }


    exitParenthesized(ctx) {
       ctx.source = ctx.help.source;
      //this.append(ctx.children);
    }

    exitExpressionElement(ctx) {
      this.append(ctx.help.source);
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;

/*const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
  return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
    switch (c) {
    case '"':
    case "'":
    case "\\":
      return "\\" + c;
    case "\n":
      return "\\n";
    case "\r":
      return "\\r";
    case "\u2028":
      return "\\u2028";
    case "\u2029":
      return "\\u2029";
    }
  });
}

const HelperType = {
  START_BLOCK : "block",
  EXPR : "single"
};

function each(ctx, renderer, lst) {
  let str = "";
  for (let i = 0; i < lst.length; i++) {
    str += renderer(lst[i]);
  }
  return str;
}

function ifExpr(ctx, renderer, e) {
  if (e) {
    return renderer(ctx);
  }

  return "";
}

function withExpr(ctx, renderer, fieldName) {
  let v = ctx[fieldName];
  let str = renderer(v);
  return str;
}

class HandlebarsCompiler extends HandlebarsParserListener {
  constructor() {
    super();
    this._inputVar = "_$ctx";
    this._outputVar = "_$result";
    this._helpers = {expr: {}, block: {}};
    this._salt = "__$";
    this._usedHelpers = [];

    // lmao look at all these stacks i added
    this._exprTypeStack = [];
    this._exprStack = [];
    this._bodyStack = [];

    this._blockNameStack = [];
    this._blockArgsStack = [];

    this.registerBlockHelper("each", each);
    this.registerBlockHelper("if", ifExpr);
    this.registerBlockHelper("with", withExpr);
  }

  registerExprHelper(name, helper) {
    this._helpers.expr[name] = helper;
  }

  registerBlockHelper(name, helper) {
    this._helpers.block[name] = helper;
  }

  pushScope() {
    let str = `var ${this._outputVar} = "";\n`;
    this._bodyStack.push(str);
  }

  popScope() {
    let str = this._bodyStack.pop();
    str += `return ${this._outputVar};\n`;
    return str;
  }

  peek() {
    return this._bodyStack[this._bodyStack.length - 1];
  }

  compile(template) {
    this._exprTypeStack = [];
    this._exprStack = [];
    this._bodyStack = [];

    // this._bodySource = `var ${this._outputVar} = "";\n`;
    this.pushScope();

    const chars = new antlr4.InputStream(template);
    const lexer = new HandlebarsLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    const tree = parser.document();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    for (let helperName in this._helpers.expr) {
      // TODO check for repeats with this._usedHelper
      let str = `var ${this._salt}${helperName} = ${this._helpers.expr[helperName].toString()};\n`;
      // this._bodySource = str.concat(this._bodySource);
      this._bodyStack[this._bodyStack.length - 1] = str.concat(this.peek());
    }

    for (let blockName in this._helpers.block) {
      let str = `var ${this._salt}${blockName} = ${this._helpers.block[blockName].toString()};\n`;
      this._bodyStack[this._bodyStack.length - 1] = str.concat(this.peek());
    }

    // this._bodySource += `return ${this._outputVar};\n`;
    let res = this.popScope();
    // console.log(this._bodySource);
    console.log(res);
    return new Function(this._inputVar, res);
     // return new Function(this._inputVar, this._bodySource);
  }


  append(expr) {
    // this._bodySource += `${this._outputVar} += ${expr};\n`;
    this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`;
  }

  enterExpressionElement(ctx) {
    this._exprStack.push([]);
    this._exprTypeStack.push(HelperType.SINGLE);
  }


  exitBlockElement(ctx) {
  }

  enterStartBlockElement(ctx) {
    this._exprStack.push([]);
    this._exprTypeStack.push(HelperType.BLOCK);
  }

  exitStartBlockElement(ctx) {
    this.pushScope();
    this._blockNameStack.push(ctx.name.text);
    this._exprTypeStack.pop();

    let args = this._exprStack.pop();


    this._blockArgsStack.push(args);
  }

  enterEndBlockElement(ctx) {
    // automatically works for null case
    let blockName = this._blockNameStack.pop();
    if (blockName != ctx.name.text) {
      throw `Block start '${blockName}' does not match the block end '${ctx.name.text}'.`
    }

    let res = this.popScope();

    let func = new Function(this._inputVar, res);

    console.log("result" + func.toString());

    let expr = `${this._salt}${ctx.name.text}(${this._inputVar}, ${func.toString()}`;
    let args = this._blockArgsStack.pop();
    for (let i = 0; i < args.length; i++) {
      expr += `, ${args[i]}`;
    }

    expr += ")";

    this.append(expr);
  }

  exitRawElement(ctx) {
    this.append(`"${escapeString(ctx.getText())}"`);
  }

  exitLiteral(ctx) {
    if (this._exprStack.length == 0) {
      this.append(ctx.getText());
    } else {
      this._exprStack[this._exprStack.length - 1].push(ctx.getText());      
    }
  }

  exitParenthesized(ctx) {
  }
  

  exitDataHelper(ctx) {
    let str = `${this._inputVar}.${ctx.getText()}`;
    if (this._exprStack.length == 0) {
      this.append(str);
    } else {
      this._exprStack[this._exprStack.length - 1].push(str);      
    }
  }

  exitHelperCall(ctx) {
    let str = `${this._salt}${ctx.id.text}(${this._inputVar}`;
    let arr = this._exprStack.pop();
    for (let i = 0; i < arr.length; i++) {
      str += `, ${arr[i]}`;
    }

    str += ")";
    this._exprStack.push([str]);
    // this.append(str);
  }



  exitExpressionElement(ctx) {
    let arr = this._exprStack.pop();
    this._exprTypeStack.pop();

    for (let i = 0; i < arr.length; i++) {
      this.append(arr[i]);
    }
  }
  
}

exports.HandlebarsCompiler = HandlebarsCompiler;*/ 
