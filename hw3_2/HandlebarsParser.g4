parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF;

element
    : rawElement
    | blockElement
    | expressionElement
    | commentElement
    ;

rawElement
	: braceElement
	| TEXT
	;

braceElement 
	: BRACE TEXT
	;

blockElement
	: START BLOCK id=ID (args+=helperElement)* END #startBlockElement
	| START CLOSE_BLOCK id=ID END #endBlockElement
	;

expressionElement
	: START help=helperElement END
	;

helperElement returns [String source]
	: OPEN_PAREN help=helperElement CLOSE_PAREN #parenthesized
	| id=ID #dataHelper
	| id=ID (args+=helperElement)+ #HelperCall //either a data lookup OR helper call
	| (INTEGER|FLOAT|STRING) #literal
	;

commentElement : START COMMENT END_COMMENT ;
