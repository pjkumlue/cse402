const ctx = {
    episodes: [
        {title: "The Phantom Menace", jedi: ["Anakin", "Qui-Gon", "Obi-Wan", "Yoda", "Mace"]},
        {title: "Attack of the Clones", jedi: ["Anakin", "Obi-Wan", "Yoda", "Mace"]},
        {title: "Revenge of the Sith", jedi: ["Obi-Wan", "Yoda", "Mace", "Luke"]},
        {title: "A New Hope", jedi: ["Obi-Wan", "Luke"]},
        {title: "The Empire Strikes Back", jedi: ["Obi-Wan", "Luke", "Yoda"]},
        {title: "Return of the Jedi", jedi: ["Anakin", "Luke", "Yoda"]},
        {title: "The Force Awakens", jedi: ["Luke", "Rey"]},
        {title: "The Last Jedi", jedi: ["Luke", "Rey"]},
    ],
};

exports.helpers = [];
exports.blockHelpers = [];
exports.ctx = ctx;
exports.description = "Example 6";
