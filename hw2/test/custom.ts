import { expect } from "chai";
import { expectQuerySequence } from "./q";
import * as q from "../src/q";
const Q = q.Q;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

	it("Ensure deep optimization", () => {
		const query = Q.join(Q.filter((x) => (x[2].match("THEFT"))).filter((x) => x[3].match("CAR PROWL")), (l, r) => (l[4] == r[4]));
		const opt = query.optimize();
		expectQuerySequence(opt, ["Join"]);
		if (opt instanceof q.JoinNode) {
			expectQuerySequence(opt.right, ["Filter"]);
		} else {
			Error("Top level node should be JoinNode.")
		}
	});

	it("Ensure deep optimization down both children, and finally the parent.", () => {
		const testData = [{ key: 1, value: 2 },
		{ key: 1, value: 3 },
		{ key: 2, value: 4 },
		{ key: 3, value: 5 },
		{ key: 4, value: 6 },
		{ key: 7, value: 9 }];
		const query = Q.filter((x) => (x.value != 5)).filter((x) => (x.value != 6)).join(Q.filter((x) => (x.value != 9)), "key");
		const opt = query.optimize();
		// console.log(opt);
		expectQuerySequence(opt, ["HashJoin"]);
		if (opt instanceof q.JoinNode) {
			//we should see the filter().filter() call condense into one node, and HashJoin become the top level.
			expectQuerySequence(opt.right, ["Filter"]);
			expectQuerySequence(opt.left, ["Filter"]);
		} else {
			Error("Top level node should be JoinNode.")
		}
		//ensure data is right
		//ensure optimizations happened as they should have. 
	});

	it("Ensure optimize alternate filter tree structure", () => {
		let tree1: q.ASTNode = new q.ThenNode(new q.ThenNode(new q.CountNode(), new q.FilterNode((x: any) => false)), new q.FilterNode((x: any) => false));
		tree1 = tree1.optimize();
		expectQuerySequence(tree1, [ "Count", "Filter"]);

		let tree2: q.ASTNode = new q.ThenNode(new q.FilterNode(null), new q.FilterNode(null));
		tree2 = tree2.optimize();
		expectQuerySequence(tree2, ["Filter"]);

		let query2 = Q.filter(null).filter(null);
		let opt2 = query2.optimize();
		expectQuerySequence(opt2, ["Filter"]);

		let query = Q.count().filter((x) => false).filter((x) => false);
		let opt = query.optimize();
		expectQuerySequence(opt, ["Count", "Filter"]);

		let query3 = Q.count().filter(null).filter(null).filter(null);
		let opt3 = query3.optimize();
		expectQuerySequence(opt3, ["Count", "Filter"]);

		// multiple branches
		let query4 = Q.count().filter(x => x > 4).filter(x => x < 10);
		let opt4 = query4.optimize();
		let testData = [1, 2, 3, 4, 14, 16, 17, 18];
		expectQuerySequence(opt4, ["Count", "Filter"]);
		expect(Q.count().execute(opt4.execute(testData))[0]).to.equal(1);
	});

	it("Ensure order of optimization correct", () => {
		let query = Q.filter(null).filter(null).filter(null).count();
		let opt = query.optimize();

		expectQuerySequence(opt, ["CountIf"]);

		let query2 = Q.filter(null).filter(null).count().count();
		let opt2 = query2.optimize();
		expectQuerySequence(opt2, ["CountIf", "Count"]);

		let query3 = Q.count().filter(null).count();
		let opt3 = query3.optimize();
		expectQuerySequence(opt3, ["Count", "CountIf"]);

		// multiple branches
		let testData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		let query4 = Q.join(Q, (x, y) => x == y).filter(x => x % 2 == 0).filter(x => x > 7).count();
		let opt4 = query4.optimize();
		expectQuerySequence(opt4, ["Join", "CountIf"]);
		expect(opt4.execute(testData)[0]).to.equal(2);
	});

	it("Stress test execution on data", () => {
		const testData = [];
		const testData2 = [];

		const pred3 = x => x % 5 == 0;
		const pred4 = x => x % 9 == 0;
		const expected3 = [];
		for (let i = 0; i < 1000; i++) {
			let value1 = Math.floor(Math.random() * 100000)
			let value2 = Math.floor(Math.random() * 100000)
			testData.push(value1);
			testData2.push(value2);
		}

		for (let v1 of testData) {
			// use same data
			for (let v2 of testData) {
				if (pred3(v1) && pred4(v2)) {
					expected3.push([v1, v2]);
				}
			}
		}

		let pred1 = x => x > 10000;
		let pred2 = x => x % 2 == 0;
		let pred5 = (x, y) => pred3(x) && pred4(y);
		
		let query = Q.filter(pred1).filter(pred2);
		let opt = query.optimize();
		
		const expected1 = testData.filter(pred1).filter(pred2);
		const expected2 = testData2.filter(pred2).filter(pred1);
		
		let res1 = opt.execute(testData);
		let res2 = opt.execute(testData2);
		expect(expected1.length).to.equal(res1.length);
		expect(expected2.length).to.equal(res2.length);

		let query2 = Q.filter(pred3).join(Q.filter(pred4), pred5);
		let opt2 = query2.optimize();
		
		expect(expected3.length).to.equal(query2.execute(testData).length);
		expect(expected3.length).to.equal(opt2.execute(testData).length);
	});
});
