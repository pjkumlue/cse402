import * as q from "./q";
import {crimeData as cd} from "../test/data";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.FilterNode((x) => x[2].match("THEFT"));
export const autoTheftsQuery = new q.FilterNode((x) => x[2].match("VEH-THEFT") && x[3].match("^MOTOR VEHICLE THEFT$"));

//// 1.4 clean the data

// export const cleanupQuery = new q.IdNode();
export const cleanupQuery = new q.ApplyNode((x) => {
    return {
        description: x[2],
        category: x[3],
        area: x[4],
        date: x[5]
    }
});

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply((x) => {
    return {
        description: x[2],
        category: x[3],
        area: x[4],
        date: x[5]
    }
});

export const theftsQuery2 = Q.filter((x) => x.description.match("THEFT"));
export const autoTheftsQuery2 = Q.filter((x) => x["description"].match("VEH-THEFT") &&
                                        x["category"].match("^MOTOR VEHICLE THEFT$"));

//// 4 put your queries here (remember to export them for use in tests)

//filters all records for thefts, and joins the records together based on happening in the same city.
export const theftByCityQuery = Q.filter((x) => x.description.match("THEFT")).join(Q, ((l,r) => l[4] == r[5]));

//Filters all records for thefts, and then applies the data to just return the more specific crime and the city it occurred in.
export const consolidatedTheftsQuery = Q.filter((x) => x[2].match("THEFT")).apply((x) => ({"Crime": x[3] , "City":x[4]}));
