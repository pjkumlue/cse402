/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;
    private parent: ASTNode;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {

        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        let f;
        if (typeof (relation) === "string") {
            f = (datum: { left: any, right: any }) => datum.left[relation] == datum.right[relation];
            // since fucntions are first class objects, we can assign fields to them. We store 
            // the field we joined on for our optimizer to retrieve later, when we detect
            // a field join we can convert it into a hash join node.
            f.field = relation;
        } else {
            f = (datum: { left: any, right: any }) => relation(datum.left, datum.right);
        }

        return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode(f)), new ApplyNode((datum: { left: any, right: any }) => (Object.assign(datum.right, datum.left))));


        // PJ -- here is the original, unoptimizied version if you're interested in it. Otherwise just delete it.
        /*        //optimize this.
                if (typeof(relation) === "string") {
                    return new ThenNode (
                        new ThenNode(
                            new CartesianProductNode(this, query), 
                            new FilterNode((datum: {left: any, right: any}) => datum.left[relation] == datum.right[relation])), 
                        new ApplyNode((datum: {left: any, right: any}) => (Object.assign(datum.right, datum.left))));
                } else { 
                    return new ThenNode (
                        new ThenNode(
                            new CartesianProductNode(this, query), 
                            new FilterNode((datum: {left: any, right: any}) => relation(datum.left, datum.right))), 
                        new ApplyNode((datum: {left: any, right: any}) => (Object.assign(datum.right, datum.left))));
                }
            }*/
    }
}



// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        // return cloned version
        return [...data];
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        let res = [];
        for (let v of data) {
            if (this.predicate(v)) {
                res.push(v);
            }
        }
        return res;
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        let res = this.first.execute(data);
        return this.second.execute(res);
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;
    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback;
    }

    execute(data: any[]): any {
        let res = [];
        for (let v of data) {
            res.push(this.callback(v));
        }
        return res;
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        let res = [];
        res.push(data.length);
        return res;
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

/* 
    if we are executing the left subtree, then the last execution
    before switching to the right subtree is the first 
 */

function GetRightmost(root: ThenNode): [ASTNode, ASTNode, ASTNode] {
    let curr: ThenNode = root;
    let parent = null;
    while (curr.second instanceof ThenNode) {
        parent = curr;
        curr = curr.second;
    }

    return [parent, curr, curr.second];
}

function GetLeftmost(root: ThenNode): [ASTNode, ASTNode, ASTNode] {
    let curr: ThenNode = root;
    let parent = null;
    while (curr.first instanceof ThenNode) {
        parent = curr;
        curr = curr.first;
    }    

    return [parent, curr, curr.first];
}

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {

    //this seems almost too simple...maybe there's some edge cases not being covered.
    if (this.first instanceof FilterNode && this.second instanceof FilterNode) {
        return new FilterNode((x) => (this.first as FilterNode).predicate(x) && (this.second as FilterNode).predicate(x));
    } else if (this.first instanceof ThenNode && this.second instanceof FilterNode) {
        let curr: ThenNode = this;
        if (curr.first instanceof ThenNode) {
            let child: ThenNode = curr.first;

            while (child.second instanceof ThenNode) {
                curr = child;
                child = child.second;
            }

            if (child.second instanceof FilterNode) {
                let filterNode: FilterNode = child.second;
                let predicate = this.second.predicate;
                if (curr == this) {
                    curr.first = child.first;
                } else {
                    curr.second = child.first;
                }
                this.second = new FilterNode((x) => (predicate(x) && filterNode.predicate(x)));
                return this;
            }
        }
    } else if (this.first instanceof FilterNode && this.second instanceof ThenNode) {
        let curr: ThenNode = this;
        //A.)
        if (curr.second instanceof ThenNode) {
            let child: ThenNode = curr.second;

            while (child.first instanceof ThenNode) {
                curr = child;
                child = child.first;
            }

            if (child.first instanceof FilterNode) {
                let filterNode: FilterNode = child.first;
                let predicate = this.first.predicate;

                //wait, isn't this first if statement unreachable? 
                //Loop should ALWAYS iterate once.
                if (curr == this) {
                    curr.second = child.second;
                } else {
                    curr.first = child.second;
                    //do we need to null out child.second? won't we crawl the same node twice?
                }

                //Do we have replicated child nodes in the bottom-most then?
                this.first = new FilterNode((x) => (predicate(x) && filterNode.predicate(x)));
                return this;
            }
        }
    }
    return null;
});
//// 2.2 internal node types and CountIf

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof FilterNode && this.second instanceof CountNode) {
        return new CountIfNode(this.first.predicate);
    /*} else if (this.first instanceof ThenNode && this.second instanceof CountNode) {
        if (this.first.second instanceof FilterNode) {
            return new ThenNode(this.first.first, new CountIfNode(this.first.second.predicate));
        }
    }*/
    } else if (this.first instanceof ThenNode && this.second instanceof CountNode) {
        let curr: ThenNode = this;
        if (curr.first instanceof ThenNode) {
            let child: ThenNode = curr.first;

            while (child.second instanceof ThenNode) {
                curr = child;
                child = child.second;
            }

            if (child.second instanceof FilterNode) {
                let filterNode: FilterNode = child.second;
                if (curr == this) {
                    curr.first = child.first;
                } else {
                    curr.second = child.first;
                }
                this.second = new CountIfNode(filterNode.predicate);
                return this;
            }
        }


    } else if (this.first instanceof FilterNode && this.second instanceof ThenNode) {
        let curr: ThenNode = this;
        if (curr.second instanceof ThenNode) {
            let child: ThenNode = curr.second;

            while (child.first instanceof ThenNode) {
                curr = child;
                child = child.first;
            }

            if (child.first instanceof CountNode) {;
                if (curr == this) {
                    curr.second = child.second;
                } else {
                    curr.first = child.second;
                }
                this.first = new CountIfNode(this.first.predicate);
                return this;
            }
        }
    }
    return null;
});

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        let count = 0;
        for (let v of data) {
            count += this.predicate(v) ? 1 : 0;
        }
        return [count];
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        let leftres = this.left.execute(data);
        let rightres = this.right.execute(data);
        let res = [];
        for (let u of leftres) {
            for (let v of rightres) {
                res.push({ left: u, right: v });
            }
        }
        return res;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(), this.right.optimize());
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    condition: (datum: { left: any, right: any }) => boolean;

    constructor(left: ASTNode, right: ASTNode, condition: (datum: { left: any, right: any }) => boolean) {
        super("Join");
        this.left = left;
        this.right = right;
        this.condition = condition;
    }

    execute(data: any[]) {
        let leftres = this.left.execute(data);
        let rightres = this.right.execute(data);
        let res = [];
        for (let u of leftres) {
            for (let v of rightres) {
                if (this.condition({ left: u, right: v })) {
                    res.push(Object.assign(v, u));
                }
            }
        }
        return res;
    }

    optimize(): ASTNode {
        return new JoinNode(this.left.optimize(), this.right.optimize(), this.condition);
    }
}

//Optimizations for the Join and Hash Join Nodes.
AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    // These first two if statements are pattern detection for unoptimized JoinNodes..
    if (this.first instanceof ThenNode && this.second instanceof ApplyNode) {
        if (this.first.first instanceof CartesianProductNode && this.first.second instanceof FilterNode) {

            // This if statement checks that the field we set back in the join() call is non-null. If it is, 
            // That means we have a field join. Thus we can convert into the more efficient HashJoin Node.
            if (this.first.second.predicate["field"] != null) {
                return new HashJoinNode(this.first.second.predicate["field"], this.first.first.left, this.first.first.right);
            }
            return new JoinNode(this.first.first.left, this.first.first.right, this.first.second.predicate);
        }
    }
    return null;
});


/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    field: string;

    constructor(field: string, left: ASTNode, right: ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.field = field;
    }

    execute(data: any[]) {
        //First, get the results from left and right tables
        let leftres = this.left.execute(data);
        let rightres = this.right.execute(data);
        let res = [];

        //then we build their respective hash tables as described in the spec.
        let leftable = this.buildTable(leftres);
        let righttable = this.buildTable(rightres);

        //here comes the painful part! merging the tables.
        for (var key in righttable) {
            //this jank if check is to make sure we don't grab some k-v pairs that are inherited from the Object class.
            if (righttable.hasOwnProperty(key)) {

                //Does the left table have this key?
                if (leftable[key] != null) {

                    ///Yes. Must merge the data before pushing. We must
                    //do this for each object pair in the left table[key]'s
                    //and right table[key]'s bucket.
                    for (let rightdat of righttable[key]) {
                        for (let leftdat of leftable[key]) {
                            let temp = rightdat;
                            Object.assign(temp, leftdat);
                            res.push(temp)
                        }
                    }
                } else {
                    //oherwise, we just push the rightdat onto the results.
                    for (let rightdat of righttable[key]) {
                        res.push(rightdat);
                    }

                }
            }
        }

        //Now we run one last loop, and just collect any records
        //from the left table's that are not found in the right table's
        for (var key in leftable) {
            if (leftable.hasOwnProperty(key)) {
                if (righttable[key] == null) {
                    for (let leftdat of leftable[key]) {
                        res.push(leftdat);
                    }
                }
            }
        }
        return res;
    }

    // Utility method. Builds the Hash table as described in the spec.
    // This builds the table mapping an object's this.field value back to the object itself.
    // If the mapping is not unique (two object's this.field value are the same), we create a
    //bucket where we store both of them. 
    private buildTable(data: any[]): any {
        let res = [];

        for (let u of data) {
            if (res[u[this.field]] == null) {
                res[u[this.field]] = [];
            }
            res[u[this.field]].push(u);
        }
        return res;

    }

    optimize(): ASTNode {
        return new HashJoinNode(this.field, this.left.optimize(), this.right.optimize());
    }
}
