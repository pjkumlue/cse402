import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("test match on array of digits", () => {
        const e2 = new EngExp().startOfLine().match("[")
        .zeroOrMore(new EngExp().digit().zeroOrMore().match(",").maybe(" "))
        .match("]").asRegExp();
        expect(e2.test("[123,343, 434,]")).to.be.true;
        expect(e2.test("[123, 343a, 434, ]")).to.be.false;
        // put the body of the test here
        const e = new EngExp().match("a").asRegExp();

        const result = e.exec("a");

        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
        expect(e.test("a")).to.be.true;
        expect(result[0]).to.be.equal("a");
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.
    it("test should throw mismatch error", () => {
        let f = () => {
            const e = new EngExp().startOfLine().beginLevel().beginLevel()
            .match("a")
            .endLevel().asRegExp();
        }

        let f2 = () => {
            const e = new EngExp().startOfLine().beginCapture().beginCapture()
            .match("a")
            .endCapture().asRegExp();
        }

        let f3 = () => {
            const e = new EngExp().startOfLine().beginCapture().beginCapture()
            .match("a")
            .endLevel().endLevel().asRegExp();
        }

        let f4 = () => {
            const e = new EngExp().startOfLine().beginLevel().beginLevel()
            .match("a")
            .endCapture().endCapture().asRegExp();
        }

        expect(f).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    })

    it("disjunctive date pattern with captures", () => {
        const month = new EngExp()
            .match("Jan").or("Feb").or("Mar").or("Apr")
            .or("May").or("Jun").or("Jul").or("Aug")
            .or("Sep").or("Oct").or("Nov").or("Dec");
        const e = new EngExp()
            .startOfLine().beginCapture()
            .digit().repeated(1, 2)
            .then("/")
            .then(new EngExp().digit().repeated(1, 2))
            .then("/")
            .then(new EngExp().digit().repeated(2, 4))
            .or(
             new EngExp()
                .digit().repeated(1, 2)
                .then(" ")
                .then(month)
                .then(" ")
                .then(new EngExp().digit().repeated(2, 4))
            )
            .endCapture().endOfLine()
            .asRegExp();
        
        expect(e.test("12/25/2015")).to.be.true;
        expect(e.test("25 Dec 2015")).to.be.true;
        expect(e.test("11/11/11/11")).to.be.false;
        expect(e.test("111 Dec 9999")).to.be.false;
    });

    it("test named captures on phone number", () => {
        let e = new EngExp().beginCapture().digit().repeated(3, 3).endCapture()
        .match("-").beginCapture().digit().repeated(3, 3).endCapture().match("-")
        .beginCapture().digit().repeated(4, 4).endCapture().asRegExp();
        let res = e.exec("123-453-4999");
        
        expect(res[1]).to.be.equal("123");
        expect(res[2]).to.be.equal("453");
        expect(res[3]).to.be.equal("4999");
    });

    it("should parse a clock time", () => {
        let e = new EngExp().startOfLine().beginLevel().digit().repeated(2, 2)
        .or(new EngExp().digit()).endLevel()
        .match(":").beginLevel().digit().repeated(2, 2).endLevel()
        .maybe(new EngExp().match("am").or("pm")).endOfLine().asRegExp();

        expect(e.test("12:01")).to.be.true;
        expect(e.test("1:01")).to.be.true;
        expect(e.test("312:01")).to.be.false;
        expect(e.test("13:01am")).to.be.true;
        expect(e.test("1:05pm")).to.be.true;
        expect(e.test("7:45fm")).to.be.false;
    });

});
