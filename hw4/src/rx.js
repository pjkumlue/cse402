
function identity(d) {
    return d;
}

function alwaysTrue(d) {
    return true;
}

function compose(f, g) {
    return (d) => {
        return f(g(d));
    }
}

class Stream {
    constructor() {
        this.callbacks = [];
        this.next = null;
    }

    appendStream(strm) {
        let s = this;
        while (s.next != null) {
            s = s.next;
        }
        s.next = strm;

        // don't need to do for root stream since it needs to store callback
        // and also already calls push on children
        if (s != this) {
            s.callbacks = [];
            s.subscribe((d) => {
                strm._push(d);
            });
        }

        // subscribe last stream to call the root streams callbacks
        strm.subscribe(((d) => {
            for (let i in this.callbacks) {
                this.callbacks[i](d);
            }
        }).bind(this));
    }

    subscribe(callback) {
        this.callbacks.push(callback);
        return this;
    }

    _push(data) {
        if (this.next == null) {
            for (let i in this.callbacks) {
                this.callbacks[i](data);
            }
        } else {
            this.next._push(data);
        }

    }

    _push_many(lst) {
        for (let lstIndex in lst) {
            this._push(lst[lstIndex]);
        }
    }

    first() {
        let fs = new FirstStream();
        this.appendStream(fs);
        return this;
    }

    map(f) {
        let m = new MapStream(f);
        this.appendStream(m);
        return this;
    }

    filter(pred) {
        let str = new FilterStream(pred);
        this.appendStream(str);
        return this;
    }

    distinctUntilChanged() {
        let d = new DucStream();
        this.appendStream(d);
        return this;
    }

    flatten() {
        let f = new FlattenStream();
        this.appendStream(f);
        return this;
    }

    scan(f, acc) {
        let s = new ScanStream(f, acc);
        this.appendStream(s);
        return this;
    }

    join(B) {
        let joinStream = new JoinStream(B);
        this.appendStream(joinStream);
        return this;
    }

    combine() {
        let c = new CombineStream();
        this.appendStream(c);
        return this;
    }

    zip(B, f) {
        let z = new ZipStream(B, f);
        this.appendStream(z);
        return this;
    }

    throttle(n) {
        this.appendStream(new ThrottleStream(n));
        return this;
    }

    unique(f) {
        this.appendStream(new UniqueStream(f));
        return this;
    }

    url(u) {
        $.get(u, (data) => {
            this._push(data);
        }, "json");
        return this;
    }

    latest() {
        let l = new LatestStream();
        this.appendStream(l);
        return this;
    }

    static timer(n) {
        let s = new Stream();
        setInterval(() => {
            s._push(new Date());
        }, n);
        return s;
    }

    static dom(element, eventName) {
        let s = new Stream();
        element.on(eventName, (e) => {
            s._push(e);
        })
        return s;
    }

    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            dataType: "jsonp",
            jsonp: "callback",
            data: _data,
            success: function(data) { out._push(data[1]); }
        });
        return out;
    }
    
}

class FirstStream extends Stream {
    constructor() {
        super();
        this.hasPushed = false;
    }

    _push(data) {
        if (!this.hasPushed) {
            super._push(data);
            this.hasPushed = true;
        }
    }
}

class MapStream extends Stream {
    constructor(f) {
        super();
        this.func = f;
    }

    _push(data) {
        super._push(this.func(data));
    }
}

class FilterStream extends Stream {
    constructor(pred) {
        super();
        this.pred = pred;
    }

    _push(data) {
        if (this.pred(data)) {
            super._push(data);
        }
    }
}

class DucStream extends Stream {
    constructor() {
        super();
        this.lastValue = null;
        this.hasPushed = false;
    }

    _push(data) {
        if (this.lastValue != data || !this.hasPushed) {
            super._push(data);
            this.lastValue = data;
        }
        this.hasPushed = true;
    }
}

class FlattenStream extends Stream {
    constructor() {
        super();
    }

    _push(data) {
        if (data instanceof Array) {
            for (let i in data) {
                super._push(data[i]);
            }
        } else {
            super._push(data);
        }
    }
}

class ScanStream extends Stream {
    constructor(f, a) {
        super();
        this.func = f;
        this.acc = a;
    }

    _push(data) {
        this.acc = this.func(this.acc, data);
        super._push(this.acc);
    }
}

class JoinStream extends Stream {
    constructor(B) {
        super();
        this.otherStream = B;
        this.otherStream.subscribe(((data) => {
            super._push(data);
        }).bind(this));
    }

    _push(data) {
        super._push(data);
    }
}

class CombineStream extends Stream {
    constructor() {
        super();
    }

    // assume A produces streams
    _push(strm) {
        strm.subscribe((data) => {
            super._push(data);
        });
    }
}

class ZipStream extends Stream {
    constructor(B, f) {
        super();
        this.otherStream = B;
        this.lastA = null;
        this.lastB = null;

        this.aProduced = false;
        this.bProduced = false;

        this.func = f;
        B.subscribe(((data) => {
            this.bProduced = true;
            this.lastB = data;
            if (this.aProduced && this.bProduced) {
                super._push(this.func(this.lastA, this.lastB));
            }
        }).bind(this));
    }

    _push(data) {
        this.aProduced = true;
        this.lastA = data;
        if (this.aProduced && this.bProduced) {
            super._push(this.func(this.lastA, this.lastB));
        }
    }
}

class ThrottleStream extends Stream {
    constructor(n) {
        super();
        this.interval = n;
        this.receiveNext = true;
    }

    _push(data) {
        if (this.receiveNext) {
            super._push(data);
            this.receiveNext = false;
            setTimeout(() => {
                this.receiveNext = true;
            }, this.interval);
        }
    }
}

class UniqueStream extends Stream {
    constructor(f) {
        super();
        this.hashFunc = f;
        this.table = {};
    }

    _push(data) {
        let v = this.hashFunc(data);
        if (!this.table[v]) {
            super._push(data);
            this.table[v] = true;
        }
    }
}

class LatestStream extends Stream {
    constructor() {
        super();
        this.currentStream = null;
        this.oldStream = null;
        this.currHasPushed = false;
    }

    _push(data) {
        this.oldStream = this.currentStream;
        this.currentStream = data;
        this.currHasPushed = false;
        if (data instanceof Stream) {
            console.log("HERE");
            data.subscribe(((d) => {
                if (data == this.oldStream && !this.currHasPushed ) {
                    super._push(d);
                } else if (data == this.currentStream) {
                    this.currHasPushed = true;
                    super._push(d);
                } 
            })
            .bind(this));
        }
    }
}

// class Stream {
//     constructor() {
//         this.callbacks = [];
//         this.next = null;
//     }


//     subscribe(callback) {
//         this.callbacks.push(callback);
//     }

//     _push(data) {
//         for (let i in this.callbacks) {
//             this.callbacks[i](data);
//         }
//     }

//     _push_many(lst) {
//         for (let lstIndex in lst) {
//             this._push(lst[lstIndex]);
//         }
//     }

//     first() {
//         let firstElement = null;
//         let p = this._push.bind(this);
//         this._push = (d) => {
//             if (firstElement == null) {
//                 p(d);
//                 firstElement = d;
//             }
//         }
//         return this;
//     }

//     map(f) {
//         // let newStream = new MapStream(f, this.callbacks);
//         // // newStream.callbacks = this.callbacks;
//         // this.next = newStream;

//         // this.callbacks = [];
//         // this.callbacks.push((d) => {
//         //     console.log("yepep");
//         //     newStream._push(d);
//         // });
//         let p = this._push.bind(this);
//         this._push = (d) => {
//             p(f(d));
//         }
//         return this;

//     }

//     filter(pred) {
//         let p = this._push.bind(this);
//         this._push = (d) => {
//             if (pred(d)) {
//                 p(d);
//             }
//         }
//         return this;
//     }

//     distinctUntilChanged() {
//         let p = this._push.bind(this);
//         let wasSet = false;
//         let last = null;
//         this._push = (d) => {
//             if (last != d || !wasSet) {
//                 p(d);
//             }            
//             wasSet = true;
//             last = d;
//         }
//         return this;
//     }

//     flatten() {
//         // let p = this._push.bind(this);
//         // this._push = (d) => {
//         //     console.log(d);
//         //     if (d instanceof Array) {
//         //     // if (Array.isArray(d)) {
//         //         console.log("is arr");
//         //         for (let i in d) {
//         //             p(d[i]);
//         //         }
//         //     } else {
//         //         p(d);
//         //     }
//         // }
//     }

//     scan(f, acc) {
//         let p = this._push.bind(this);
//         this._push = (d) => {
//             acc = f(d, acc);
//             p(acc);
//         }
//         return this;
//     }

//     // not sure what to do if multiple values go into the same stream in succession
//     // right now just discard
//     join(B) {
//         let out = new Stream();
//         let aNext = true;
//         let p = this._push.bind(this);
//         this._push = (d) => {
//             if (aNext) {
//                 p(d);
//                 aNext = false;
//             }
//         };
//         B.subscribe((d) => {
//             if (!aNext) {
//                 p(d);
//                 aNext = true;
//             }
//         });
//         return this;
//     }

//     combine() {

//         return this;
//     }
// }


if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    $("#time").text(new Date());
    Stream.timer(1000).subscribe((d) => {
        $("#time").text(d);
    });

    let numClicks = 0;

    $("#clicks").text(numClicks);
    Stream.dom($("#button"), "click").subscribe((d) => {
        numClicks++;
        $("#clicks").text(numClicks);
    });

    this.hasEntered = false;
    $("#mouseposition").text("");
    Stream.dom($("#mousemove"), "mousemove").subscribe((event) => {
        let x = event.pageX;
        let y = event.pageY;
        $("#mouseposition").text(x + ", " + y);
    }).throttle(1000);

    let addresses = [];
    let fire = new Stream();
    fire.flatten().unique((d) => d.id).subscribe((d) => {
        let address = d[405818852];
        addresses.push(address);
        $("#fireevents").append($("<li></li").text(address));
    });

    fire.url(FIRE911URL);
    Stream.timer(1000 * 60).subscribe((d) => {
        fire.url(FIRE911URL);
    });

    Stream.dom($("#wikipediasearch"),"keyup")
    .throttle(100)
    .map((event) => {
        let searchterm = event.target.value;
        return WIKIPEDIAGET(searchterm);
    })
    .latest()
    .subscribe((data) => {
        $("#wikipediasuggestions").empty();
        for (i in data) {
            $("#wikipediasuggestions").append($("<li></li").text(data[i]));
        }
    });


    Stream.dom($("#firesearch"), "input").subscribe((event) => {
        $("#fireevents").empty();
        let str = event.target.value;
        for (let i in addresses) {
            let addr = addresses[i];
            if (addr.includes(str)) {
                $("#fireevents").append($("<li></li").text(addr));
            }
        }
    });


    loadGuess = 10;
    loadClicks= 0;
    loadDone = false;
    Stream.dom($("#load-more"), "click").join(Stream.dom($("#load-complete"), "click")).subscribe((e) => {
        //if from load more add color to bar
        let id = e.target.id;
        if (id == "load-more" && !loadDone) {
            loadClicks++;
            if (loadClicks == loadGuess) {
                loadGuess *= 2;
            }
            $("#progress").width((loadClicks/loadGuess)*100 + "%");
        } else {
            loadDone = true;
            $("#progress").width("100%");
            $("#progress").addClass("finished");
        }

    });

    // Add your hooks to implement Parts 2-4 here.
}
