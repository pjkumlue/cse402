grammar Regex;

re
    : expression EOF
    | alt EOF
    ;

alt
    : expression ('|' expression)*
    ;

expression
    : (elems+=element)* # NormalExpression
//    | (elems+=element)* ('|' alts+=expression)* # AltExpression
    ;


element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
	: '(' c=alt ')' # parens
    | c=CHARACTER # atomic
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
